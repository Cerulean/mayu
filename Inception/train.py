# -*- coding:utf-8 -*-

import tensorflow as tf
import numpy as np

import os
import glob
import random

from tensorflow.python.platform import gfile

BOTTLE_NECK_TENSOR_NODE_COUNT = 2048

BOTTLE_NECK_TENSOR_NAME = "pool_3/_reshape:0"

INPUT_TENSOR_NAME = "DecodeJpeg/content:0"

MODEL_DIR = './model_v3'
MODEL_NAME = 'tensorflow_inception_graph.pb'

CACHE_DIR = './cache'

FLOWER_DIR = './flower_photos'

VALIDATION_RATE = 0.1
TESTING_RATE = 0.1

LEARNING_RATE = 0.01
STEPS = 4000
BATCH = 100


def create_image_list(validation_rate, testing_rate):

    image_list = []

    sub_dirs = [dirs[0] for dirs in os.walk(FLOWER_DIR)][1:]

    suffixes = ['jpg', 'jpeg', 'JPG', 'JPEG']

    for sub_dir in sub_dirs:

        base_dir_name = os.path.basename(sub_dir)

        glob_paths = []

        for suffix in suffixes:
            glob_path = os.path.join(FLOWER_DIR, base_dir_name, '*.' + suffix)
            glob_paths.extend(glob.glob(glob_path))

        if glob_paths is None:
            continue

        label_name = base_dir_name.lower()

        testing_images = []
        training_images = []
        validation_images = []

        for glob_path in glob_paths:

            base_name = os.path.basename(glob_path)

            rate = np.random.rand()

            if rate < validation_rate:
                validation_images.append(base_name)
            elif rate < testing_rate + validation_rate:
                testing_images.append(base_name)
            else:
                training_images.append(base_name)

        image_list[label_name] = {
            'dir': base_dir_name,
            'testing_images': testing_images,
            'training_images': training_images,
            'validation_images': validation_images,
        }

    return image_list


def get_image_path(image_list, image_dir, label_name, tag, index):

    images = image_list[label_name]

    target_images = images[tag]

    image_name = images[index % len(target_images)]

    label_dir = image_list['dir']

    image_path = os.path.join(image_dir, label_dir, image_name)

    return image_path


def get_bottleneck_path(images_list, label_name, index, tag):
    return get_image_path(images_list, CACHE_DIR, label_name, index, tag) + '.txt'


if __name__ == '__main__':

    create_image_list(VALIDATION_RATE, TESTING_RATE)

