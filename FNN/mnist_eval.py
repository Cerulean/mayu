# -*- coding:utf-8 -*-

import time

import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

import mnist_inference
import mnist_train

EVAL_INTERVAL_SECS = 10

def evaluate(mnist):

    with tf.Graph().as_default() as graph:

        input_x = tf.placeholder(tf.float32, [None, mnist_inference.INPUT_NODE_COUNT], name='x-input')
        input_y = tf.placeholder(tf.float32, [None, mnist_inference.OUTPUT_NODE_COUNT], name='y-input')

        output_y = mnist_inference.inference(input_x, None)

        validation_feed_dict = {input_x: mnist.validation.images,
                                input_y: mnist.validation.labels}

        correct_list = tf.equal(tf.arg_max(output_y, dimension=1),
                                tf.arg_max(input_y, dimension=1))

        accuracy = tf.reduce_mean(tf.cast(correct_list, tf.float32))

        ema_object = tf.train.ExponentialMovingAverage(mnist_train.MOVING_AVERAGE_DECAY)
        ema_vars_to_restore = ema_object.variables_to_restore()

        saver = tf.train.Saver(ema_vars_to_restore)

        while True:

            with tf.Session() as session:

                checkpoint = tf.train.get_checkpoint_state(mnist_train.MODEL_SAVE_PATH)

                if checkpoint and checkpoint.model_checkpoint_path:

                    saver.restore(session, checkpoint.model_checkpoint_path)

                    global_step = int(checkpoint.model_checkpoint_path.split('/')[-1].split('-')[-1])

                    score = session.run(accuracy, feed_dict=validation_feed_dict)

                    print 'After %d training steps, validation accuracy is %g' % (global_step, score)

                else:

                    print 'No checkpoint file found'

                    return

            time.sleep(EVAL_INTERVAL_SECS)


def main(argv=None):
    mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
    evaluate(mnist)


if __name__ == '__main__':
    tf.app.run()