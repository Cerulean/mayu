# -*- coding:utf-8 -*-

import tensorflow as tf


INPUT_NODE_COUNT = 784
HIDDEN_NODE_COUNT = 500
OUTPUT_NODE_COUNT = 10


def get_weights_vars(shape, regularizer=None):

    weights = tf.get_variable('weights',
                              shape=shape,
                              initializer=tf.truncated_normal_initializer(stddev=0.1))

    if regularizer is not None:

        tf.add_to_collection('losses', regularizer(weights))

    return weights


def inference(input_tensor, regularizer):

    with tf.variable_scope('layer_1'):

        weights = get_weights_vars([INPUT_NODE_COUNT, HIDDEN_NODE_COUNT], regularizer)

        biases = tf.get_variable('biases', [HIDDEN_NODE_COUNT], initializer=tf.constant_initializer(0.0))

        layer_1_output = tf.nn.relu(tf.matmul(input_tensor, weights) + biases)

    with tf.variable_scope('layer_2'):

        weights = get_weights_vars([HIDDEN_NODE_COUNT, OUTPUT_NODE_COUNT], regularizer)

        biases = tf.get_variable('biases', [OUTPUT_NODE_COUNT], initializer=tf.constant_initializer(0.0))

        layer_2_output = tf.matmul(layer_1_output, weights) + biases

    return layer_2_output