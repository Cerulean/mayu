# -*- coding:utf-8 -*-

import tensorflow as tf

from tensorflow.examples.tutorials.mnist import input_data

INPUT_LAYER_NODE_COUNT = 784
HIDDEN_LAYER_NODE_COUNT = 500
OUTPUT_LAYER_NODE_COUNT = 10

BATCH_SIZE = 100

BASE_LEARNING_RATE = 0.8
DECAY_LEARNING_RATE = 0.99

REGULARIZATION_RATE = 0.0001

TRAINING_STEPS = 30000

MOVING_AVERAGE_DECAY_RATE = 0.99


class MNIST(object):

    def __init__(self):

        self.mnist = input_data.read_data_sets('MNIST_data', one_hot=True)

        self.input_x = tf.placeholder(tf.float32, [None, INPUT_LAYER_NODE_COUNT], name='input-x')
        self.input_y = tf.placeholder(tf.float32, [None, OUTPUT_LAYER_NODE_COUNT], name='input-y')

        self.hidden_layer_weights = tf.Variable(tf.truncated_normal([INPUT_LAYER_NODE_COUNT, HIDDEN_LAYER_NODE_COUNT],
                                                                    stddev=0.1))
        self.hidden_layer_biases = tf.Variable(tf.constant(0.1, shape=[HIDDEN_LAYER_NODE_COUNT]))

        self.output_layer_weights = tf.Variable(tf.truncated_normal([HIDDEN_LAYER_NODE_COUNT, OUTPUT_LAYER_NODE_COUNT],
                                                                    stddev=0.1))
        self.output_layer_biases = tf.Variable(tf.constant(0.1, shape=[OUTPUT_LAYER_NODE_COUNT]))

    def calculate(self, avg_class=None):

        if avg_class is None:
            a = tf.nn.relu(tf.matmul(self.input_x, self.hidden_layer_weights) + self.hidden_layer_biases)
            y = tf.matmul(a, self.output_layer_weights) + self.output_layer_biases
            return y
        else:
            a = tf.nn.relu(tf.matmul(self.input_x, avg_class.average(self.hidden_layer_weights)) + avg_class.average(self.hidden_layer_biases))
            y = tf.matmul(a, avg_class.average(self.output_layer_weights)) + avg_class.average(self.output_layer_biases)
            return y

    def train(self):

        global_step = tf.Variable(0, trainable=False)

        ema_vars_object = tf.train.ExponentialMovingAverage(MOVING_AVERAGE_DECAY_RATE, global_step)
        ema_vars_op = ema_vars_object.apply(tf.trainable_variables())

        avg_y = self.calculate(avg_class=ema_vars_object)
        std_y = self.calculate()

        cross_entropy_mean = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=std_y,
                                                                                           labels=tf.argmax(self.input_y, 1)))

        l2_reg = tf.contrib.layers.l2_regularizer(REGULARIZATION_RATE)
        l2_reg = l2_reg(self.hidden_layer_weights) + l2_reg(self.output_layer_weights)

        loss = cross_entropy_mean + l2_reg

        learning_rate = tf.train.exponential_decay(BASE_LEARNING_RATE,
                                                   global_step,
                                                   self.mnist.train.num_examples / BATCH_SIZE,
                                                   DECAY_LEARNING_RATE)

        train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)

        with tf.control_dependencies([train_step, ema_vars_op]):
            train_op = tf.no_op(name='train')

        correct_list = tf.equal(tf.arg_max(avg_y, dimension=1), tf.argmax(self.input_y, dimension=1))

        accuracy = tf.reduce_mean(tf.cast(correct_list, tf.float32))

        with tf.Session() as session:

            tf.initialize_all_variables().run()

            validation_dic = {self.input_x: self.mnist.validation.images,
                              self.input_y: self.mnist.validation.labels}

            test_dic = {self.input_x: self.mnist.test.images,
                        self.input_y: self.mnist.test.labels}

            for step in range(TRAINING_STEPS):

                if step % 1000 == 0:
                    validation_accuracy = session.run(accuracy, feed_dict=validation_dic)

                    print "After %d steps, validation accuracy is %g" % (step, validation_accuracy)

                batch_input_x, batch_input_y = self.mnist.train.next_batch(BATCH_SIZE)

                session.run(train_op, feed_dict={self.input_x: batch_input_x,
                                                 self.input_y: batch_input_y})

        test_accuracy = session.run(accuracy, feed_dict=test_dic)

        print "After %d steps, test accuracy is %g" % (TRAINING_STEPS, test_accuracy)


if __name__ == '__main__':

    mnist = MNIST()
    mnist.train()