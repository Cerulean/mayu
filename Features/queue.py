# -*- coding:utf-8 -*-

import tensorflow as tf


if __name__ == '__main__':

    queue = tf.FIFOQueue(2, 'int32')

    queue_init = queue.enqueue_many(([0, 10], ))

    x = queue.dequeue()

    y = x + 1

    queue_increase = queue.enqueue([y])

    with tf.Session() as session:

        queue_init.run()

        for _ in range(5):
            v, _ = session.run([x, queue_increase])
            print v