# -*- coding:utf-8 -*-

import tensorflow as tf


def _int64_feature(value):
    int64_list = tf.train.Int64List(value=[value])
    return tf.train.Feature(int64_list=int64_list)


def write():
    shard_count = 2
    instance_count = 2

    for shard in range(shard_count):
        filename = ('./records/data.tfrecords-%.5d-of-%.5d' % (shard, shard_count))
        writer = tf.python_io.TFRecordWriter(filename)

        for instance in range(instance_count):
            features = tf.train.Features(feature={
                'shard': _int64_feature(shard),
                'instance': _int64_feature(instance)
            })
            example = tf.train.Example(features=features)
            writer.write(example.SerializeToString())

        writer.close()


def read():
    files = tf.train.match_filenames_once('./records/data.tfrecords-*')
    files_queue = tf.train.string_input_producer(files, shuffle=False)

    reader = tf.TFRecordReader()

    _, serialized_example = reader.read(files_queue)
    features = tf.parse_single_example(
        serialized_example,
        features={
            'shard': tf.FixedLenFeature([], tf.int64),
            'instance': tf.FixedLenFeature([], tf.int64)
        }
    )
    with tf.Session() as session:
        tf.global_variables_initializer().run()
        print session.run(files)
        coordinator = tf.train.Coordinator()
        threads = tf.train.start_queue_runners(sess=session, coord=coordinator)
        for i in range(6):
            print session.run([features['shard'], features['instance']])
        coordinator.request_stop()
        coordinator.join(threads)


if __name__ == '__main__':
    read()