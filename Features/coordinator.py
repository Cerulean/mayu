# -*- coding:utf-8 -*-

import tensorflow as tf
import numpy as np
import threading
import time

if __name__ == '__main__':

    # Define Queue.
    queue = tf.FIFOQueue(100, tf.float32)

    # Define enqueue op.
    enqueue_op = queue.enqueue([tf.random_normal([1])])

    # Define dequeue op.
    dequeue_op = queue.dequeue()

    # Define QueueRunner with 5 threads, whose op is enqueue op.
    queue_runner = tf.train.QueueRunner(queue, [enqueue_op] * 5)

    # Add QueueRunner to Default QUEUE_RUNNERS
    tf.train.add_queue_runner(queue_runner)

    with tf.Session() as session:

        coordinator = tf.train.Coordinator()

        threads = tf.train.start_queue_runners(sess=session, coord=coordinator)

        for _ in range(15):
            print "Current length of queue is %g" % queue.size().eval()
            print session.run(dequeue_op)[0]

        coordinator.request_stop()
        coordinator.join(threads)