# -*- coding:utf-8 -*-

import tensorflow as tf
import numpy as np

from tensorflow.examples.tutorials.mnist import input_data


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def write_record():

    mnist = input_data.read_data_sets('./MNIST_data', dtype=tf.uint8, one_hot=True)

    images = mnist.train.images
    labels = mnist.train.labels

    pixels = images.shape[1]

    images_count = mnist.train.num_examples

    record_path = "./output.tfrecords"

    writer = tf.python_io.TFRecordWriter(record_path)

    for index in range(images_count):
        raw_image = images[index].tostring()

        record = tf.train.Example(features=tf.train.Features(feature={
            'label': _int64_feature(np.argmax(labels[index])),
            'pixels': _int64_feature(pixels),
            'raw_images': _bytes_feature(raw_image)
        }))

        writer.write(record.SerializeToString())

    writer.close()


if __name__ == '__main__':

    reader = tf.TFRecordReader()

    record_queue = tf.train.string_input_producer(["./output.tfrecords"])

    _, serialized_record = reader.read(record_queue)

    features = tf.parse_single_example(
        serialized_record,
        features={
            'raw_images': tf.FixedLenFeature([], tf.string),
            'pixels': tf.FixedLenFeature([], tf.int64),
            'label': tf.FixedLenFeature([], tf.int64)
        }
    )

    images = tf.decode_raw(features['raw_images'], tf.uint8)
    labels = tf.cast(features['label'], tf.int32)
    pixels = tf.cast(features['pixels'], tf.int64)

    session = tf.Session()

    coordinator = tf.train.Coordinator()

    threads = tf.train.start_queue_runners(sess=session, coord=coordinator)

    for i in range(10):
        image, label, pixel = session.run([images, labels, pixels])