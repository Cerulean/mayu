# -*- coding:utf-8 -*-

import os

import numpy as np
import tensorflow as tf
from tensorflow.contrib.layers import l2_regularizer
from tensorflow.examples.tutorials.mnist import input_data

import mnist_inference

BATCH_SIZE = 100

LEARNING_RATE_BASE = 0.8
LEARNING_RATE_DECAY = 0.99

REG_LAMBDA = 0.0001

TRAINING_STEPS = 30000

MOVING_AVERAGE_DECAY = 0.99

MODEL_SAVE_PATH = './models/'
MODEL_NAME = 'mnist_model'


def train(mnist):

    # Define placeholder tensor for training.
    input_x = tf.placeholder(tf.float32,
                             [BATCH_SIZE,
                              mnist_inference.IMAGE_SIZE,
                              mnist_inference.IMAGE_SIZE,
                              mnist_inference.CHANNELS_COUNT],
                             name='x-input')

    input_y = tf.placeholder(tf.float32, [None, mnist_inference.OUTPUT_NODE_COUNT], name='y-input')

    # Define L2-Regularizer with Lambda of REG_LAMBDA.
    l2_reg = l2_regularizer(REG_LAMBDA)

    # Define operation of calculate the output.
    output_y = mnist_inference.inference(input_x, l2_reg, is_training=True)

    # Define var to count the training step.
    global_step = tf.Variable(0, trainable=False)

    # Define EMA object with MOVING_AVERAGE_DECAY and global_step
    ema_object = tf.train.ExponentialMovingAverage(MOVING_AVERAGE_DECAY, global_step)

    # Define ema operation for all trainable variables.
    ema_op = ema_object.apply(tf.trainable_variables())

    # Define mean cross entropy, where input_y is like shape of [0, 0, 1, 0, 0, 0, 0, 0]
    cross_entropy_mean = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=output_y,
                                                                                       labels=tf.argmax(input_y, 1)))
    # Define all loss.
    loss = cross_entropy_mean + tf.add_n(tf.get_collection('losses'))

    # Define exp decay learning rate.
    learning_rate = tf.train.exponential_decay(LEARNING_RATE_BASE,
                                               global_step,
                                               mnist.train.num_examples / BATCH_SIZE,
                                               LEARNING_RATE_DECAY)

    # Define train step operation.
    train_step = tf.train.GradientDescentOptimizer(learning_rate).minimize(loss, global_step=global_step)

    with tf.control_dependencies([train_step, ema_op]):
        train_op = tf.no_op(name='train')

    saver = tf.train.Saver()

    with tf.Session() as session:

        tf.global_variables_initializer().run()

        for i in range(TRAINING_STEPS):

            batch_x, batch_y = mnist.train.next_batch(BATCH_SIZE)

            batch_x = np.reshape(batch_x,
                                 (BATCH_SIZE,
                                  mnist_inference.IMAGE_SIZE,
                                  mnist_inference.IMAGE_SIZE,
                                  mnist_inference.CHANNELS_COUNT))

            _, loss_value, step = session.run([train_op, loss, global_step],
                                              feed_dict={input_x: batch_x,
                                                         input_y: batch_y})

            if i % 1000 == 0:
                print 'After %d training steps,' \
                      ' loss on training batch is %g' % (i, loss_value)

                saver.save(session, os.path.join(MODEL_SAVE_PATH, MODEL_NAME), global_step=global_step)


def main(argv=None):
    mnist = input_data.read_data_sets('MNIST_data', one_hot=True)
    train(mnist)


if __name__ == '__main__':
    tf.app.run()