# -*- coding:utf-8 -*-

import tensorflow as tf


INPUT_NODE_COUNT = 784

IMAGE_SIZE = 28

CHANNELS_COUNT = 1
LABELS_COUNT = 10

CONV_FILTER_1_DEPTH = 32
CONV_FILTER_1_WIDTH = 5
CONV_FILTER_1_HEIGHT = 5

CONV_FILTER_2_DEPTH = 64
CONV_FILTER_2_WIDTH = 5
CONV_FILTER_2_HEIGHT = 5

FNN_NODE_COUNT = 512
OUTPUT_NODE_COUNT = 10


def get_weights_vars(shape, regularizer=None):

    weights = tf.get_variable('weights',
                              shape=shape,
                              initializer=tf.truncated_normal_initializer(stddev=0.1))

    if regularizer is not None:

        tf.add_to_collection('losses', regularizer(weights))

    return weights


def inference(input_tensor, regularizer, is_training=False):

    with tf.variable_scope('layer_1_conv'):

        # Define filter size in layer 1, input size is 28 * 28 * 1, here the filter size is 5 * 5 * 32.
        layer_1_weights = tf.get_variable('weights',
                                          [CONV_FILTER_1_HEIGHT,
                                           CONV_FILTER_1_WIDTH,
                                           CHANNELS_COUNT,
                                           CONV_FILTER_1_DEPTH],
                                          initializer=tf.truncated_normal_initializer(stddev=0.1))

        layer_1_biases = tf.get_variable('biases',
                                         [CONV_FILTER_1_DEPTH],
                                         initializer=tf.constant_initializer(0.0))

        conv_1_result = tf.nn.conv2d(input_tensor,
                                     layer_1_weights,
                                     strides=[1, 1, 1, 1],
                                     padding='SAME')

        # Layer 1 output will be BATCH_SIZE * 28 * 28 * 32 (Due to the padding of SAME and stride of [1, 1])
        layer_1_output = tf.nn.relu(tf.nn.bias_add(conv_1_result, layer_1_biases))

    with tf.variable_scope('layer_2_pool'):

        # Layer 2 output will be BATCH_SIZE * 14 * 14 * 32 (Due to the padding of SAME of stride of [2, 2])
        # The output size is (28 / 2, 28 / 2)
        layer_2_output = tf.nn.max_pool(layer_1_output,
                                        ksize=[1, 2, 2, 1],
                                        strides=[1, 2, 2, 1],
                                        padding='SAME')

    with tf.variable_scope('layer_3_conv'):

        # Define filter size in layer 3, input size is 14 * 14 * 32, here the filter size is 5 * 5 * 64.
        layer_3_weights = tf.get_variable('weights',
                                          [CONV_FILTER_2_HEIGHT,
                                           CONV_FILTER_2_WIDTH,
                                           CONV_FILTER_1_DEPTH,
                                           CONV_FILTER_2_DEPTH],
                                          initializer=tf.truncated_normal_initializer(stddev=0.1))

        layer_3_biases = tf.get_variable('biases',
                                         [CONV_FILTER_2_DEPTH],
                                         initializer=tf.constant_initializer(0.0))

        conv_3_result = tf.nn.conv2d(layer_2_output,
                                     layer_3_weights,
                                     strides=[1, 1, 1, 1],
                                     padding='SAME')

        # Layer 3 output will be BATCH_SIZE * 14 * 14 * 64 (Due to the padding of SAME and stride of [1, 1])
        layer_3_output = tf.nn.relu(tf.nn.bias_add(conv_3_result, layer_3_biases))

    with tf.variable_scope('layer_4_pool'):

        # Layer 4 output will be BATCH_SIZE * 7 * 7 * 64 (Due to the padding of SAME of stride of [2, 2])
        # The output size is (14 / 2, 14 / 2)
        layer_4_output = tf.nn.max_pool(layer_3_output,
                                        ksize=[1, 2, 2, 1],
                                        strides=[1, 2, 2, 1],
                                        padding='SAME')

    layer_4_shape = layer_4_output.get_shape().as_list()

    layer_5_input_tensor_width = layer_4_shape[1] * layer_4_shape[2] * layer_4_shape[3]

    layer_5_input_tensor = tf.reshape(layer_4_output, [layer_4_shape[0], layer_5_input_tensor_width])

    with tf.variable_scope('layer_5_nn'):

        layer_5_weights = tf.get_variable('weights',
                                          [layer_5_input_tensor_width, FNN_NODE_COUNT],
                                          initializer=tf.truncated_normal_initializer(stddev=0.1))

        if regularizer is not None:
            tf.add_to_collection('losses', regularizer(layer_5_weights))

        layer_5_biases = tf.get_variable('biases',
                                         [FNN_NODE_COUNT],
                                         initializer=tf.constant_initializer(0.1))

        layer_5_output = tf.nn.relu(tf.matmul(layer_5_input_tensor, layer_5_weights) + layer_5_biases)

        if is_training:
            layer_5_output = tf.nn.dropout(layer_5_output, 0.5)

    with tf.variable_scope('layer_6_nn'):

        layer_6_weights = tf.get_variable('weights',
                                          [FNN_NODE_COUNT, OUTPUT_NODE_COUNT],
                                          initializer=tf.truncated_normal_initializer(stddev=0.1))

        if regularizer is not None:
            tf.add_to_collection('losses', regularizer(layer_6_weights))

        layer_6_biases = tf.get_variable('biases',
                                         [OUTPUT_NODE_COUNT],
                                         initializer=tf.constant_initializer(0.1))

        layer_6_output = tf.matmul(layer_5_output, layer_6_weights) + layer_6_biases

    return layer_6_output
